#!/bin/bash
##################################################################
# Settings
# Which Interface do you want to check/fix
wlan='wlan0'
# Which address do you want to ping to see if the network interface is alive (typically use your routers IP).
pingip='192.168.0.1'
# How many restart tries before reboot
MAX_TRIES=4
##################################################################
echo "Performing Network check for $wlan"

touch retries || exit
retries=$(cat retries)

/bin/ping -c 1 -I $wlan $pingip > /dev/null 2> /dev/null
if [ $? -ge 1 ] ; then
    echo 'wlan0 is down, restarting wlan0 please wait'
    /sbin/ifdown $wlan
    sleep 5
    /sbin/ifup --force $wlan

    let "retries = $retries + 1"
    echo "$retries" > retries
    if [ $retries -gt $MAX_TRIES ]
    then
        #reboot as last effort
        /sbin/reboot
    fi
else
    echo 'wlan0 is up'
    echo "0" > retries
fi